package GmailTest.GmailApp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class StepDef 
{
	public static WebDriver obj=null;

@Given("^Open url$")
public void open_url() throws Throwable {
	System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\geckodriver.exe");
	obj=new FirefoxDriver();
	obj.manage().window().maximize();
	obj.get("https://www.gmail.com");
	
}

@Then("^Fill UserId$")
public void fill_UserId() throws Throwable {
	obj.findElement(By.id("Email")).sendKeys("nivedha@gmail.com");
	obj.findElement(By.id("next")).click();
	Thread.sleep(2000);
}

@Then("^Fill Password$")
public void fill_Password() throws Throwable {
	obj.findElement(By.id("Passwd")).sendKeys("N@k$#@TR@");
	
}

@And("^Hit on login$")
public void hit_on_login() throws Throwable {
	obj.findElement(By.id("signIn")).click();
	Thread.sleep(5000);
	   }

@And("^Quit Browser$")
public void quit_Browser() throws Throwable {
	obj.quit();
	
}


}
