$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/myGmailApp.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Gmail Application Login Logout Automation"
    }
  ],
  "line": 3,
  "name": "Gmail Application",
  "description": "",
  "id": "gmail-application",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 5,
  "name": "Gmail Login",
  "description": "",
  "id": "gmail-application;gmail-login",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "Open url",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Fill UserId",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "Fill Password",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Hit on login",
  "keyword": "And "
});
formatter.match({
  "location": "StepDef.open_url()"
});
formatter.result({
  "duration": 8064113411,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.fill_UserId()"
});
formatter.result({
  "duration": 2682754539,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.fill_Password()"
});
formatter.result({
  "duration": 82335041,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.hit_on_login()"
});
formatter.result({
  "duration": 5516753259,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Gmail Close",
  "description": "",
  "id": "gmail-application;gmail-close",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 12,
  "name": "Quit Browser",
  "keyword": "And "
});
formatter.match({
  "location": "StepDef.quit_Browser()"
});
formatter.result({
  "duration": 4210804210,
  "status": "passed"
});
});